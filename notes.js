// using vex dialog for adding new notes or editing existing ones
vex.defaultOptions.className = 'vex-theme-wireframe';

// using es6 
// using const instead of let for the variable to not be later reassigned
const initial = () => { 
   const notesInit = {
      notes: [
         {id: 1, title: 'Car', text: 'add gasoline, change wheels, wash and pay the maintenance'},
         {id: 2, title: 'Home', text: 'get home by 8pm, do homework with Haley, cook dinner'},
         {id: 3, title: 'Groceries', text: 'milk, butter, bread, herbal tea, hummus and orange juice'}
      ],
      currentlySelected: 1
   };
   
   // retrieve notes from local storage
   const retrievedObject = localStorage.getItem('notesObject');
   const parsedRetrievedObject = JSON.parse(retrievedObject);

   const noNotesInLocalStorage = parsedRetrievedObject === null || parsedRetrievedObject.notes.length == 0;

   if(noNotesInLocalStorage) {
      return notesInit;
   } else {
      return parsedRetrievedObject;
   }
};

// reducer
const noteReducer = (state = initial(), action) => {
   let newState = {};
   let newNote = [];

   switch (action.type) {
   case 'ADD_NOTE':
      console.log(action);
      newNote = [...state.notes, action];
      newState = Object.assign({}, state, {notes: newNote});
      // put the notes into the local storage
      localStorage.setItem('notesObject', JSON.stringify(newState));
      return newState;

   case 'DELETE_NOTE':
      console.log(action);
      newNote = [
         ...state.notes.slice(0, action.id),
         ...state.notes.slice(action.id + 1)
      ];
      newState = Object.assign({}, state, {notes: newNote});
      // put the notes into the local storage
      localStorage.setItem('notesObject', JSON.stringify(newState));
      return newState;

   case 'CHANGE_SELECTED_NOTE':
      console.log(action);
      return Object.assign({}, state, {currentlySelected: action.currentlySelected});

   case 'EDIT_NOTE':
      console.log(action);
      newNote = [
         ...state.notes.slice(0, action.id),
         action,
         ...state.notes.slice(action.id + 1)
      ];
      newState = Object.assign({}, state, {notes: newNote} );
      // put the notes into the local storage
      localStorage.setItem('notesObject', JSON.stringify(newState));
      return newState;

   default:
      return state;

   }
};

const {createStore} = Redux;
const store = createStore(noteReducer);

const NoteList = React.createClass({
   getInitialState(){
      return {
         currentlySelected: store.getState().currentlySelected
      };
   },

   editPrompt() {
      let current = store.getState().notes[this.state.currentlySelected];

      vex.close();

      vex.dialog.open({
         message: 'Edit Note',
         input: `<input name="editTitle" type="text" value="${current.title}" />
               <textarea name="editText" rows="7">${current.text}</textarea>`,
         buttons: [
            $.extend({}, vex.dialog.buttons.NO, {
               text: 'Cancel',
               click: () => vex.close()
            }),
            $.extend({}, vex.dialog.buttons.YES, {
               text: 'OK'
            })
         ],
         callback: data => {
            if(data){
               store.dispatch({
                  type: 'EDIT_NOTE',
                  id: this.state.currentlySelected,
                  title: data.editTitle,
                  text: data.editText
               });
            }
         }
      });
   },

   infoPrompt(i) {
      store.dispatch({
         type: 'CHANGE_SELECTED_NOTE',
         currentlySelected: i
      });

      this.setState({currentlySelected: i}, () => {
         let current = store.getState().notes[this.state.currentlySelected];

         vex.dialog.open({
            message: `<h1>${current.title}</h1> \n ${current.text}`,
            input: '',
            buttons: [
               $.extend({}, vex.dialog.buttons.YES, {
                  text: 'Cancel'
               }), $.extend({}, vex.dialog.buttons.NO, {
                  text: 'Edit',
                  click: () => this.editPrompt()
               })
            ]
         });
      });
   },

   render(){
      return (
         <ul className="col-xs-12">{
            this.props.notes.map((note, i) => {
               return (<li className="jumbotron list__jumbo row" key={i}>
                  <div>
                     <span className="list__close-button col-xs-1" onClick={ () => store.dispatch({id: i, type: 'DELETE_NOTE'}) }>&times;</span>
                  </div>
                  <div>
                     <span className="list__title col-xs-11" onClick={this.infoPrompt.bind(this, i)}>{note.title}</span>
                  </div>
               </li>);
            })
         }</ul>
      );
   }
});

const Header = (props) => {
   return (
      <div className="header-main">
         <h5 className="header__mainTitle"><a href="https://github.com/alanbuchanan">Notes For Everything</a></h5>
         <div className="header__addButton-container">
            <a onClick={props.save} target="_blank"><span className="glyphicon glyphicon-plus-sign"></span></a>
         </div>
      </div>
   );
};

const NoteBox = React.createClass({
   savePrompt() {
      vex.dialog.open({
         message: 'Add a Note',
         input: `<input name="userTitle" type="text" placeholder="Name"/>
            <textarea name="userText" type="text" rows="7" placeholder="Notes/Text"/>`,
         
         callback: data => {
            if(data) {
               store.dispatch({
                  type: 'ADD_NOTE',
                  id: Date.now(),
                  title: data.userTitle,
                  text: data.userText
               });
            }
         }
      });
   },

   render() {
      return (
         <div>
            <Header save={this.savePrompt}/>
            <NoteList notes={store.getState().notes}/>
         </div>
      );
   }
});

const render = () => ReactDOM.render(<NoteBox />, document.getElementById('root'));
store.subscribe(render);
render();